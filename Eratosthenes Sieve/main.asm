;======= Crivello di Eratostene======
; inizializza la memoria scrivendo tutti gli interi n con 0<=n<=1000, partendo da 0x0060 incluso. 
; applica il crivello di eratostene scandendo tutta la tabella e rimpiazzando i numeri composti con il valore 0
; compatta l'array in modo che alla fine i primi (a partire da 1,incluso tra i primi) si trovino a partire 
; dall'indirizzo 0x0060 (inizio di sram) incluso e poi tutti in sequenza. I numeri sono memorizzati nella sram in little endian.
;

reset:
 rjmp start

start:
 ldi r16, low(ramend)
 ldi r17,high(ramend)
 out spl,r16
 out sph,r17
 rcall azzera
 rcall initialize
 rcall crivello
 rcall compatta
 nop
fine:
 rjmp fine


azzera:;azzero tutta la sram eccetto l'ultimo byte, che contiene la stack dove c'� l'indirizzo a cui devo ritornare al main 
 ldi r29,high(0x0060) ;y tengono l indirizzo
 ldi r28,low(0x0060)
 ldi r16,low(0x085d) ;r17:r16 tengono valore max
 ldi r17,high(0x085d)
 ldi r18,0
 mov r0,r18
cicloazzera:
 mov r25,r17
 mov r24,r16
 sub r24,r28
 sbc r25,r29

 brlo fineazzera
 st Y+,r0
 st Y+,r0
 rjmp cicloazzera
fineazzera:
 ret
 

initialize:
 ldi r30,0x0
 ldi r31,0x0 ; z tiene il valore
 ldi r29,high(0x0060) ;y tengono l indirizzo
 ldi r28,low(0x0060)
 ldi r16,low(1001) ;r17:r16 tengono valore max
 ldi r17,high(1001)
ciclo:
 mov r25,r17
 mov r24,r16
 sub r24,r30
 sbc r25,r31
 or r24,r25
 breq fineciclo
 st Y+,r30
 st Y+,r31
 ld r9,z+
 rjmp ciclo
fineciclo:
 ret

crivello:
 ldi r30,low(0x0064)
 ldi r31,high(0x0064);indirizo di due
primociclo:
 mov r21,r29            ;condizione di uscita
 mov r20,r28
 sub r20,r30
 sbc r21,r31
 or r20,r21
 breq finecrivello
 mov r26,r30;copio l indirizzo nella variabile del ciclo secondo (potrebbe servirmo dopo)
 mov r27,r31
 ld r24,z+;carico il dato all indirizzo e aggiorno z
 ld r25,z+
 mov r22,r24;vedo se il dato � zero
 or r22,r25
 breq primociclo
 lsl r24;moltiplico per due il dato: ognuno occupa due byte e dopo dovro sommarlo a un indirizzo (nel secondo ciclo)
 rol r25
secondociclo:
 add r26,r24;sommo dato a indirizzo base
 adc r27,r25
 mov r21,r27            ;condizione di uscita
 mov r20,r26
 sub r20,r28
 sbc r21,r29
 brsh primociclo
 clr r0
 st X+,r0
 st X,r0
 ld r9,-X;mi serve per effetto collaterale: x lo voglio aggiornare a mano io allinizio del ciclo
 rjmp secondociclo
finecrivello:
 ret


compatta:
 ldi r26,low(0x0060)
 ldi r27,high(0x0060);indirizzo di 0. y sara il writing pointer
 ldi r30,low(0x0062);indirizzo di 1. z sara il reading pointer
 ldi r31,high(0x0062)
ciclolettura:
 mov r21,r29            ;condizione di uscita (in y c'� l'indirizzo max)
 mov r20,r28
 sub r20,r30
 sbc r21,r31
 or r20,r21
 breq finecompatta
 ld r24,z+
 ld r25,z+
 mov r23,r24
 or r23,r25
 breq ciclolettura
 st X+,r24
 st X+,r25
 rjmp ciclolettura
finecompatta:
 ret
