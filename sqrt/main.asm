;====Programma per il calcolo della radice quadrata di a. arrotonda a parte intera inferiore o superiore=======
reset:
 rjmp start
start:
 ldi r16,low(ramend)
 ldi r17,high(ramend)
 out spl,r16
 out sph,r17
.equ a=40001
 ldi r16,low(a)
 ldi r17,high(a)
 mov r0, r16
 mov r1,r17

 rcall sqrt
 nop
fine:
 rjmp fine
;========
;calcolo sqrt(a), con a in r1:r0, con metodo di newton. risultato in r6
; funziona cos�: in r1:r0 lascio a. faccio un ciclo while mantenendo tre variabili da 16 bit:
; la prima � in r3:r2 e tiene il valore x_i, la seconda � in r5:r4 e tiene x_(i+1) e la terza
; � in r7;r6 e tiene x_(i+2). a ogni ciclo controllo se per caso x_(i+2) � uguale a x_(i+1) o
; a x_i. se questo � il caso ritorno x_(i+2) e termino. altrimenti shifto le tre variabili 
; calcolando col metodo di newton x_(i+4) e ripeto il ciclo.
sqrt:
 push r2
 push r3
 push r4
 push r5
 push r8
 push r9
 ldi r17,high(65535)
 ldi r16,low(65535)
 sub r16,r0
 sbc r17,r1
 brne secondotest
 ldi r16,0xFF
 mov r6,r16
 clr r7
 rjmp finesqrt
secondotest:
 mov r8,r1
 or r8,r0
 brne bene
 clr r6
 clr r7
 rjmp finesqrt
bene:
 mov r3,r1
 mov r2,r0
 clr r5
 clr r4
 clr r7
 clr r6
 push r0
 push r1
 push r2
 push r3
 push r6
 push r7
 rcall divisione ;fa r1:r0/r3:r2 e mette il quoziente in r7:r6
 mov r5, r7
 mov r4,r6
 pop r7
 pop r6
 pop r3
 pop r2
 pop r1
 pop r0
 add r4,r2
 adc r5,r3

 lsr r5
 ror r4
ciclo:
 push r0
 push r1
 push r2
 push r3
 mov r2,r4
 mov r3,r5
 rcall divisione
 pop r3
 pop r2
 pop r1
 pop r0
 add r6,r4
 adc r7,r5
 lsr r7
 ror r6
 mov r8,r6
 mov r9,r7
 sub r8,r4
 sbc r9,r5
 or r8,r9
 breq finesqrt
 mov r8,r6
 mov r9,r7
 sub r8,r2
 sbc r9,r3
 or r8,r9
 breq finesqrt
 mov r2,r4
 mov r3,r5
 mov r4,r6
 mov r5,r7
 rjmp ciclo

finesqrt:
 pop r9
 pop r8
 pop r5
 pop r4
 pop r3
 pop r2
 ret

;===========
;r1:r0 dividendo, alla fine resto
;r3:r2 divisore
;r7:r6 quoziente
;c viene settato a 1 sse divisore=0

divisione:
 push r4
 push r5
 push r8
 push r16
 mov r8,r3
 or r8,r2 ;controllo se divisore=0
 brne benediv ;divisore diverso da zero
 sec ;divisore=0: setto c e ritorno
 ret
benediv:
 clr r4 ;inizializzo registri
 clr r5
 clr r6
 clr r7
 ldi r16,17;contatore: devo verificare se q sta in p 1+16 volte
ciclodiv:
 tst r3
 brne nosubb
 tst r2
 brne nosubb
 cp r1,r5
 brcs nosubb ;se r5>r1 non sottrai
 brne subb ;se r5<r1 sottrai. altrimenti guardo l'ultimo byte:
 cp r0,r4
 brcs nosubb
subb: ;sottraggo, setto il carry che poi mettera un uno nel risultato e e muovo divisore a destra
 sub r0,r4
 sbc r1,r5
 lsr r3
 ror r2
 ror r5
 ror r4
 sec
 rjmp shift
nosubb: ;sposto divisore a destra e setto il carry a zer0, che poi andra nel risultato
 lsr r3
 ror r2
 ror r5
 ror r4
 clc 
shift: ;"accumulo" bits nel risultato, prendendo il bit dal carry
 rol r6
 rol r7
 dec r16 ;ripeto
 breq finediv; nota: siccome decremento prima e poi confronto con zero, cosi per come � scritta eseguo tutto 17 volte (valore iniziale di r16)
 rjmp ciclodiv
finediv:
 clc;c=1 sse divisore =0, caso gia trattato. dunque ripulisco c perche verosimilmente a sto punto ha un valore a caso
 pop r16
 pop r8
 pop r5
 pop r4
 ret




 



