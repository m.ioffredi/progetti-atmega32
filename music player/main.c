//Leonardo Tesi, Marco Ioffredi, Leonardo Giaquinta



#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

unsigned int cicli;//quante volte deve arrivare ocr0 al compare match
unsigned int istDur;//c'� salvato gli impulsi da contare nell'ocr0 per arrivare al wrap
unsigned int istFreq;//c'� salvato gli impulsi per calcolare la frequenza della nota
unsigned int puntatore=0;//dove � arrivato a leggere
char *canzone;
extern void InizPorteTimer(void);
extern void initString(void);



//----------------------------------------------------------------FUNZIONI-PER-IL-CALCOLO-DELLA-NOTA----------------------------------------------------

void codifica(){
	
		if(canzone[puntatore]=='\0'){//se fossimo arrivati a fine canzone rimando il puntatore alla stringa a 0 e fermo i timer per silenziarli (controllarla che � delicata)
			puntatore=0;
			TCCR0&=0b11111000;//spengo i timer
			TCCR1B&=0b11111000;
		}
	
	
	unsigned int nota=0;
	nota=(canzone[puntatore]-0x30)*10+canzone[puntatore+1]-0x30;//decodifico il buffer convertendo il numero da carattere a intero per inserirlo nello switch
	
	switch(nota){//decodifico le note, il file canzone � presente in "inizializza canzone.c"
		case 1:				//LA-1
		istFreq=16759;
		break;
		
		case 2:				//LA#-1
		istFreq=15817;
		break;
		
		case 3:				//SI-1
		istFreq=12418;
		break;
		
		case 4:				//DO
		istFreq=14094;
		break;
		
		case 5:				//DO#
		istFreq=13301;
		break;
		
		case 6:				//RE
		istFreq=12553;
		break;
		
		case 7:				//RE#
		istFreq=11851;
		break;
		
		case 8:				//MI
		istFreq=11186;
		break;
		
		case 9:				//FA
		istFreq=10558;
		break;
		
		case 10:			//FA#
		istFreq=9956;
		break;
		
		case 11:			//SOL
		istFreq=9405;
		break;
		
		case 12:			//SOL#
		istFreq=8878;
		break;
		
		case 13:			//LA
		istFreq=8379;
		break;
		
		case 14:			//LA#
		istFreq=7908;
		break;
		
		case 15:			//SI
		istFreq=7465;
		break;
		
		case 16:			//DO+1
		istFreq=7046;
		break;
		
		case 17:			//DO#+1
		istFreq=6650;
		break;
		
		case 18:			//RE+1
		istFreq=6278;
		break;
		
		case 19:			//RE#+1
		istFreq=5925;
		break;
		
		case 20:			//MI+1
		istFreq=5592;
		break;
		
		case 21:			//FA+1
		istFreq=5278;
		break;
		
		case 22:			//FA#+1
		istFreq=4982;
		break;
		
		case 23:			//SOL+1
		istFreq=4703;
		break;
		
		case 24:			//SOL#+1
		istFreq=4439;
		break;
		
		case 25:			//LA+1
		istFreq=4190;
		break;
		
		case 26:			//LA#+1
		istFreq=3955;
		break;
		
		case 27:			//SI+1
		istFreq=3732;
		break;
		
		case 28:			//pausa
		istFreq=0;
		break;
	}
	
	switch(canzone[puntatore+2])//decodifico la durata delle note presenti nello spartito, ci fossero altre "note" bisognerebbe decodificare anche quelle
	{
		case '8'://un ottavo
			cicli=5;
			istDur=216;
		break;
		
		case '1'://un quarto
			cicli=9;
			istDur=240;
		break;
		
		case '2'://due quarti
			cicli=18;
			istDur=240;
		break;
		
		case '3'://tre ottavi
			cicli=36;
			istDur=240;
		break;
	}
	
	OCR1AH=((istFreq-istFreq%256)/256)%256;//scorro il numero di 8 bit cosi la parte alta ora � ai bit meno significativi
	OCR1AL=istFreq%256;	//faccio il resto per avere gli 8 bit meno significativi
	OCR0=istDur;
	puntatore=puntatore+4;//serve per andare a leggere la nota successiva
}

//----------------------------------------------------------------GESTIONE-DEGLI-INTERRUPT--------------------------------------------------------------



ISR(INT0_vect){//gestisce il pulsante play pause
	if(TCCR0%2==0){
		TCCR0|=0b00000101;//riattivo i timer
		TCCR1B|=0b00000001;
	}
	else{
		TCCR0&=0b11111000;//spengo i timer
		TCCR1B&=0b11111000;
	}	
}

ISR(INT1_vect){//serve a gestire il pulsante per fare ripartire la canzone da capo
	TCCR0|=0b00000101;//riattivo i timer
	TCCR1B|=0b00000001;
	puntatore=0;
	codifica();//deve prendere la prima nota e caricarla nei timer
}

ISR(TIMER0_COMP_vect){//serve a contare dopo quanto va cambiata la nota
	if(cicli==0){
		codifica();//se i wrap sono finiti allora si mette la nota successiva
	}
	else{
		cicli--;//se i wrap non sono abbastanza se ne conta uno in pi� che � avvenuto
	}
}

//------------------------------------------------------------------------MAIN-----------------------------------------------------------------

int main(void)
{
    initString();//inizializzo la stringa
	InizPorteTimer();//inizializzo il uC
    while (1){}//Il corpo del programma � vuoto perch� il sistema � interamente asincrono quindi ogni funzione � gestita da handler
}


