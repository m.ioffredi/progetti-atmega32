//Leonardo Tesi, Marco Ioffredi, Leonardo Giaquinta

/*
Ogni nota � codificata con 3 numeri, i primi 2 rappresentano la nota a partire da il la dell'ottava inferiore alla centrale, la 28esima � la pausa.
 Il terzo numero rappresenta la durata della nota, 8 per un ottavo, 1 per un quarto, 2 per due quarti e 3 per tre ottavi.
*/
#include <avr/pgmspace.h>


extern char *canzone;
void initString(void){
	canzone=PSTR("068 098 138 188 048 088 138 168 038 068 098 158 018 048 098 138 038 068 118 158 018 068 098 138 038 088 118 158 048 098 138 168 212 202 182 162 152 132 152 162 212 202 182 162 152 132 152 162 061 091 131 111 152 162 152 132 182 202 182 162 181 211 251 131 151 111 131 131 111 181 182 168 188 168 188 088 048 138 088 098 ");
	
}