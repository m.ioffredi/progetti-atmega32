//Leonardo Tesi, Marco Ioffredi, Leonardo Giaquinta


#include <avr/io.h>

.section .text
.global InizPorteTimer
		

InizPorteTimer :

	//inizializzo le porte I/O (con i pull-up i bit di ingresso saranno sempre ad 1 
	//quindi bisogna attivare i timer quando si fa il falling-edge

	//------------------------------------INIZIALIZZAZIONE-PORTE-INPUT-E-OUTPUT----------------------------

	in r16,_SFR_IO_ADDR(SFIOR)//leggo il registro dove � presente il bit pud per abilitare i pull-up e lo mando a 0 per attivarli
	andi r16,0b11111011
	out _SFR_IO_ADDR(SFIOR),r16//ricarico sfior con solo quel bit modificato

	in r16,_SFR_IO_ADDR(DDRD)//carico in ddrd solo il bit per i miei pin, quindi leggo,faccio la or sui bit e sovrascrivo
	ori r16,0b00100000//ho mandato in output il bit 5 ed i teoria c'� gi� il segnale oc1a
	andi r16,0b11110011
	out _SFR_IO_ADDR(DDRD),r16//imposto ocr1a come output e i pin 2 e 3 come interrupt dei pulsanti in ingresso

	ldi r16,0b00101100
	out _SFR_IO_ADDR(PORTD),r16//attivo i pin dell'ocr e dei 2 interrupt

	//-----------------------------------------INIZIALIZZAZIONE-TIMER---------------------------------

	ldi r16,0b01000000//com1ax=01,com1bx=00,foc1x=00,wgm11=0,wgm10=0
	out _SFR_IO_ADDR(TCCR1A),r16

	ldi r16,0b00001001//noise=0,icrh/l=0,bit5=0 nonConnesso,wgm13=0,wgm12=1;cs1x=000(messo a zero ad inizializazione poi va attivato 001)
	out _SFR_IO_ADDR(TCCR1B),R16

	ldi r16,0x01
	out _SFR_IO_ADDR(OCR1AH),R16//lo inizializzo ad un valore a caso in modo tale da farlo partire con una frequenza nota
	out _SFR_IO_ADDR(OCR1AL),R16

	ldi r16,0b00001101//foc0=0,wgm0x=10,com0x=00,cs0x=000(poi dopo va messo a 101)
	out _SFR_IO_ADDR(TCCR0),r16

	ldi r16,0x01
	out _SFR_IO_ADDR(OCR0),R16//inizializzato per far partire l'handler la prima volta

	in r16,_SFR_IO_ADDR(TIMSK) //leggo e carico in r16 i bit del registro per poter modificare solo quelli del timer 1
	andi r16,0b11000010//ticie1=0,ocie1x=00,toie1=0,ocie0=1,toie=0
	ori r16,0b00000010//per essere certi di avere l'1
	out _SFR_IO_ADDR(TIMSK),R16 //risalvo nel registro

	clr r16
	//caricamento su registri della prima nota e del primo tempo

	//--------------------------------------ABILITAZIONE-INTERRUPT-ESTERNI-------------------------------

	in r16,_SFR_IO_ADDR(GICR)
	ori r16,0b11000000
	out _SFR_IO_ADDR(GICR),r16 
	
	in r16,_SFR_IO_ADDR(MCUCR)//carico mcucr in memoria
	andi r16,0b11110000//pulisco i bit che mi servono a settare il tio di interrupt
	ori r16,0b00001010 //imposto in falling edge i 2 interrupt
	out _SFR_IO_ADDR(MCUCR),r16//riscrivo il registro aggiornato

	sei

ret